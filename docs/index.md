Hello!

This is an example blog for the FabZero-Design Course.

You can edit it on [Gitlab](http://gitlab.fabcloud.org). The software used turns simple text files
written in [Markdown](https://en.wikipedia.org/wiki/Markdown) format, into the site you are navigating.

Each time you change a page using the Gitlab interface the site is rebuilt and all the changes published
in few minutes.

If this is your site, go on and edit this page clicking the Gitlab link on the upper right, changing the text below and deleting this.

No worries, you can't break anything, all the changes you make are saved under [Version Control](https://en.wikipedia.org/wiki/Version_control) using [GIT](https://git-scm.com/book/en/v2/Getting-Started-About-Version-Control). This means that you have all the different versions of your page saved and available all the time in the Gitlab interface.

## About me

![](images/avatar-photo.jpg)

Hi! I am Emma Brewer. I am an art director & graphic designer based in New York City working on branding, visual identities, editorial design and web design.

Visit this website to see my work!

## My background

I was born in a nice city called..

## Previous work

I'm a paragraph. Edit the page on Gitlab to add your own text and edit me.  I’m a great place for you to tell a story and let your users know a little more about you.​

### Project A

This is an image from an external site:

![This is the image caption](https://images.unsplash.com/photo-1512436991641-6745cdb1723f?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=ad25f4eb5444edddb0c5fb252a7f1dce&auto=format&fit=crop&w=900&q=80)

While this is an image from the assets/images folder. Never use absolute paths (starting with /) when linking local images, always relative.

![This is another caption](images/sample-photo.jpg)
